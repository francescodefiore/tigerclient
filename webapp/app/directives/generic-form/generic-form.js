CREEMapp.directive('genericForm', 

function() 
{
	
	return {
	
    controller: ['$scope', 'CreemSettings','BuildingsFactory','EPIFactory', 'DatiGeneraliForm', 'ModalService',

		function($scope, CreemSettings, BuildingsFactory, EPIFactory, DatiGeneraliForm, ModalService)
		{
			var cloneObject = function (object) {
				return jQuery.extend(true, {}, object);
			};

			$scope.currentTab = 'directives/generic-form/generic-form.html';

			$scope.onSubmit = function() {
					var modalOptions = {
						closeButtonText: 'Annulla',
						actionButtonText: 'OK',
						headerText: 'Attenzione',
						bodyText: 'Conferma Aggiornamento Dati Immobili?'
					};

					ModalService.showModal({}, modalOptions).then(function (result) {
							var datigeneraliform = new DatiGeneraliForm($scope.datigenerali);
							
							datigeneraliform.$save();					
					});
					

			  }
			  
			var refreshData = function()
			{					
					var datigenerali = EPIFactory.getDatiGenerali().query(
						function(){
							$scope.datigenerali = datigenerali;							
						}
					);
			}

			$scope.$on('settingsUpdated', function (event, data) { refreshData(); } );

			if(CreemSettings.selectedbuildings.length > 0) {
				refreshData();
			}
			

		}],

		templateUrl: 'directives/generic-form/generic-form.html'
  };

});



	
	