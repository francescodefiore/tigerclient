CREEMapp.directive('shopSelection', function() {

	  return {		
		
		templateUrl: 'directives/shop-selection.html',
				
		controller: ['$scope', 'AnagraficaFactory', 'AppSettings','$cookies', 
			function($scope, AnagraficaFactory, AppSettings,$cookies) {
							
							$scope.selected_shop;
																										
							$scope.shops = AnagraficaFactory.getAllShops().query(
								function(){
									if(AppSettings.selectdShops.length == 0)
									{	
										AppSettings.setShops($scope.shops[0]);
									}	
									$scope.selected_shop = AppSettings.selectdShops[0];	
								}
							);

							$scope.setShop = function() 
							{													
								AppSettings.selectdShops($scope.selected_building);
							}
												
						}
		],
		
		link: function(scope, element)
			  { 		
					
					$(element).find('#shops_list').val(scope.selected_building);										
					scope.$watch('shops', function(newValue, oldValue) {																	
										if (newValue.length != 0)																				
										{
											$(element).find('#shops_list').chosen();
										}
										
								}, true);	
								
				}
				
		
		
	  }
	});
	
	