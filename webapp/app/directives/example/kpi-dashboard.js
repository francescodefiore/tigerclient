CREEMapp.directive('kpiDashboard', 

function() 
{

	return {    
    controller: ['$scope', 'CreemSettings', 'KPIFactory','EPIFactory',
	
		function($scope, CreemSettings, KPIFactory, EPIFactory) 
		{			
							
			
			var createGauge = function(title, unit, divchart, kpivalue, bench, tollerance)
			{
				
				var budgetchart = new Highcharts.Chart({
				
				chart: {
					type: 'gauge',
					renderTo: divchart,
					plotBackgroundColor: null,
					plotBackgroundImage: null,
					plotBorderWidth: 0,
					plotShadow: false
				},
				
				credits: {
					enabled: false
				},

				title: {
							text: title,
							style:{								
								fontSize: '12px'
							}
						},
						
				subtitle: {
							text: unit,
							style:{								
								fontSize: '10px'
							}
				},

				pane: {
					startAngle: -150,
					endAngle: 150,
					background: [{
						backgroundColor: {
							linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
							stops: [
								[0, '#FFF'],
								[1, '#333']
							]
						},
						borderWidth: 0,
						outerRadius: '109%'
					}, {
						backgroundColor: {
							linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
							stops: [
								[0, '#333'],
								[1, '#FFF']
							]
						},
						borderWidth: 1,
						outerRadius: '107%'
					}, {
						
					}, {
						backgroundColor: '#DDD',
						borderWidth: 0,
						outerRadius: '105%',
						innerRadius: '103%'
					}]
				},
				

				// the value axis
				yAxis: {
					min: 0,
					max: 4*bench,

					minorTickInterval: 'auto',
					minorTickWidth: 1,
					minorTickLength: 10,
					minorTickPosition: 'inside',
					minorTickColor: '#666',

					tickPixelInterval: 30,
					tickWidth: 2,
					tickPosition: 'inside',
					tickLength: 10,
					tickColor: '#666',
					labels: {
						step: 2,
						rotation: 'auto'
					},
					title: {
						text: unit
					},
					plotBands: [{
						from: 0,
						to: bench,
						color: '#55BF3B' // green
					}, {
						from: bench,
						to: 2*(bench*(1+tollerance/100.0)),
						color: '#DDDF0D' // yellow
					}, {
						from: 2*(bench*(1+tollerance/100.0)),
						to: 4*bench,
						color: '#DF5353' // red
					}]
				},				

				series: [{
					name:  title,
					data: [kpivalue]
				}]
			});
			}					


			var refreshData = function ()
			{		
					$scope.potenza_picco = 0.0;
					$scope.consumo_medio = 0.0;
					$scope.episn = 0.0;
					
					$scope.bench_potenza_picco = 0.0;
					$scope.bench_consumo_medio = 0.0;
					$scope.bench_episn = 0.0;
							
					$scope.tollerance_potenza_picco = 0.0;
					$scope.tollerance_consumo_medio = 0.0;
					$scope.tollerance_episn = 0.0;
					

					$scope.building = CreemSettings.selectedbuildings[0];
					var data = KPIFactory.getCurrentMonthKPI($scope.building.idImmobile).query(
						function()
						{
							$scope.kpi = data;																			
							
							if($scope.kpi.length > 0)
							{
								for(var i=0; i<$scope.kpi.length; i++)
								{
									if( data[i].Kpi == "KpiMensileReqF001" )
									{
										$scope.potenza_picco = data[i].valoreKpi;
										$scope.bench_potenza_picco = data[i].valoreBench;
										$scope.tollerance_potenza_picco = data[i].tolleranza;
										$scope.diff_potenza_picco = parseFloat((data[i].valoreKpi/data[i].valoreBench - 1)*100).toFixed(2);
										
									}
									if(data[i].Kpi == "KpiMensileReqF002")
									{
										$scope.consumo_medio = data[i].valoreKpi;
										$scope.bench_consumo_medio = data[i].valoreBench;
										$scope.tollerance_consumo_medio = data[i].tolleranza;
										$scope.diff_consumo_medio = parseFloat((data[i].valoreKpi/data[i].valoreBench - 1)*100).toFixed(2);
									}
								}

							}

							
							var epi = EPIFactory.getEPI($scope.building.idImmobile, 2014).query(
								function()
								{
									var kpi;
									if(epi.length != 0)
									{	
										
										kpi = epi[0];
																							
										$scope.episn = kpi.EPI_SN;																	
										$scope.bench_episn = kpi.EPI_SN_Bench;	

										$scope.tollerance_episn = 20;							
									
										$scope.diff_episn = parseFloat(($scope.episn/$scope.bench_episn - 1)*100).toFixed(2);									
									}
									
									createGauge("Potenza di Picco", "kW", "gauge_potenzapicco", $scope.potenza_picco, $scope.bench_potenza_picco, $scope.tollerance_potenza_picco);
									createGauge("Consumo al mq", "kWh/mq", "gauge_consumomedio", $scope.consumo_medio, $scope.bench_consumo_medio, $scope.tollerance_consumo_medio);							
									createGauge("Epi Superficie netta", "kWh/mq", "gauge_epi", $scope.episn, $scope.bench_episn, $scope.tollerance_episn);	
									
								}
								
							);
							
							

						}
					
					);

			}
			
			$scope.$on('settingsUpdated', function (event, data) { refreshData(); } );
			
			if(CreemSettings.selectedbuildings.length > 0 || CreemSettings.selectedCities.length > 0 || CreemSettings.selectedClusters.length > 0)
				refreshData();
	
			
			
		}],
		
		templateUrl: 'directives/kpi-dashboard.html'
  };
  
});
	
	