CREEMapp.directive('coalbalanceChart', 

function() 
{

	return {
    
    controller: ['$scope', 'CreemSettings', 'MonthlyConsumptionsFactory','ManutenzioneFactory', 'ngTableParams',
	
		function($scope, CreemSettings, MonthlyConsumptionsFactory, ManutenzioneFactory, ngTableParams)
		{			
					
			var seriesCounter = 0;
			$scope.dataset = [];
			$scope.data = {};
			$scope.dataM = {};
			
			var createChart = function(title, stitle)
			{
				var monthlyconsumptionschart = new Highcharts.Chart({
				chart: {
						renderTo: 'coalbalancechart',
						type: 'column'
					},
					credits: {
						enabled: false
					},
				
					title: {
						text: title
					},
					subtitle: {
						text: stitle
					},
					xAxis: {
						categories: [
							'2013',
							'2014'
						],
						crosshair: true
					},
					yAxis: {
						min: 0,
						title: {
							text: 'Emissioni di CO2 (ton)'
						}
					},
					tooltip: {
						headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
						pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
							'<td style="padding:0"><b>{point.y:.2f} ton </b> (<i>{point.percentage:.0f} %</i>)</b></td></tr>',
						footerFormat: '</table>',
						shared: true,
						useHTML: true
					},
					plotOptions: {
						column: {
							pointPadding: 0.2,
							borderWidth: 0,
							stacking: 'value'
						} 
					},
					series: $scope.dataset
				});
			}
							

			var getdata = function () 
			{
				
					var data = [];
					var annoDa = 2013;
					var annoA = 2014;
			
					data = MonthlyConsumptionsFactory.getMonthlyConsumptions().query( function(){ prepareChart(data); });
					
					var dataM = ManutenzioneFactory.getCO2Manutenzione(annoDa,annoA).query(function()
					{
						$scope.dataM.annoDaCo2T = 0;
						$scope.dataM.annoDaCo2M = 0;
						$scope.dataM.annoACo2T = 0;
						$scope.dataM.annoACo2M = 0;
						
						for(var i = 0; i<dataM.length;i++)
						{
							if(dataM[i].anno == annoDa && dataM[i].tipo == 'ticket')
								$scope.dataM.annoDaCo2T = dataM[i].co2;
								
							if(dataM[i].anno == annoDa && dataM[i].tipo == 'ms')
								$scope.dataM.annoDaCo2M = dataM[i].co2;
								
							if(dataM[i].anno == annoA && dataM[i].tipo == 'ticket')
								$scope.dataM.annoACo2T = dataM[i].co2;

							if(dataM[i].anno == annoA && dataM[i].tipo == 'ms')
								$scope.dataM.annoACo2M = dataM[i].co2;
								
						}
						$scope.dataM.annoDaCo2Tot = $scope.dataM.annoDaCo2T + $scope.dataM.annoDaCo2M;
						$scope.dataM.annoACo2Tot = $scope.dataM.annoACo2T + $scope.dataM.annoACo2M;
						$scope.dataM.co2TTotPerc = (($scope.dataM.annoDaCo2T - $scope.dataM.annoACo2T)*100)/($scope.dataM.annoDaCo2T == 0 ? 1 : $scope.dataM.annoDaCo2T);
						$scope.dataM.co2MTotPerc = (($scope.dataM.annoDaCo2M - $scope.dataM.annoACo2M)*100)/($scope.dataM.annoDaCo2M == 0 ? 1 : $scope.dataM.annoDaCo2M);
						$scope.dataM.percTot = $scope.dataM.co2TTotPerc + $scope.dataM.co2MTotPerc;
					});

			}
						
			var prepareChart = function(data)
			{				
				
				
				var consumptions = {};
				$scope.data['_2013'] = [0, 0, 0, 0, 0];
				$scope.data['_2014'] = [0, 0, 0, 0, 0];
				$scope.data['Fonte'] = ['Gasolio', 'Elettricità', 'Gas Naturale', 'GPL', 'Totale CO2 emessa'];
				$scope.data['_2013vs2014'] = [0, 0, 0, 0, 0];
								
				
				var currentTime = new Date();
				for(var i=0; i<data.length; i++)
				{
					if(data[i].anno < currentTime.getFullYear() - 1 ) 
					{	
						$scope.data['_' + data[i].anno][4] = data[i].gen + data[i].feb + data[i].mar + data[i].apr + data[i].mag + data[i].giu + data[i].lug + data[i].ago + data[i].sett + data[i].ott + data[i].nov + data[i].dic + $scope.data['_' + data[i].anno][4];												
					}													
				}
				
				
				
				
				$scope.data['_2013'][0] = 0;
				$scope.data['_2013'][1] = $scope.data['_2013'][4]*0.001*0.4332;
				$scope.data['_2013'][2] = 20.60*1000*1.95;
				$scope.data['_2013'][3] = 0;
				$scope.data['_2013'][4] = $scope.data['_2013'][1] + $scope.data['_2013'][2];
				
				
				$scope.data['_2014'][0] = 0;
				$scope.data['_2014'][1] = $scope.data['_2014'][4]*0.001*0.4332;
				$scope.data['_2014'][2] = 24.17*1000*1.95;
				$scope.data['_2014'][3] = 0;
				$scope.data['_2014'][4] = $scope.data['_2014'][1] + $scope.data['_2014'][2];
				
				console.log($scope.data);
				
				$scope.data['_2013vs2014'] = [ (($scope.data['_2013'][0] - $scope.data['_2014'][0])*100)/$scope.data['_2013'][0], (($scope.data['_2013'][1] - $scope.data['_2014'][1])*100)/$scope.data['_2013'][1], (($scope.data['_2013'][2] - $scope.data['_2014'][2])*100)/$scope.data['_2013'][2], (($scope.data['_2013'][3] - $scope.data['_2014'][3])*100)/$scope.data['_2013'][3], (($scope.data['_2013'][4] - $scope.data['_2014'][4])*100)/$scope.data['_2013'][4]];

				$scope.dataset = [
				{
					name: 'Gasolio',
					data: [$scope.data['_2013'][0], $scope.data['_2014'][0]]					
				},				
				{
					name: 'Elettricità',
					data: [$scope.data['_2013'][1], $scope.data['_2014'][1]]
				},
				{	name: 'Gas Naturale',
					data: [$scope.data['_2013'][2], $scope.data['_2014'][2]]
				},
				{	name: 'GPL',
					data: [$scope.data['_2013'][3], $scope.data['_2014'][3]]
				}
				];		
				
				createChart('Emissioni di CO2 degli immobili CREEM ', '');											
			}
			
			$scope.cols = [
					{ field: "Fonte", title: "Fonte", show: true},
					{ field: "_2013", title: "2013", show: true},
					{ field: "_2014", title: "2014", show: true},
					{ field: "_2013vs2014", title: "% CO<sub>2</sub> 2013 vs 2014", show: true}					
				];
				
				
			$scope.tableParams = new ngTableParams({
							page: 1, // show first page
							count: 10, // count per page,							
							data: $scope.data
					});
				
			var refreshData = function ()
			{										
					$scope.dataset.splice(0, $scope.dataset.length);		

					getdata();

			}

			refreshData();
   

		}],
		
		templateUrl: 'directives/coalbalance-chart.html',
		scope: false
  };
  
});
	
	