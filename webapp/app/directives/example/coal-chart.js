CREEMapp.directive('coalChart', 

function() 
{

	return {
    
    controller: ['$scope', 'CreemSettings', 'MonthlyConsumptionsFactory',
	
		function($scope, CreemSettings, MonthlyConsumptionsFactory)
		{			
					
			var seriesCounter = 0;
			$scope.dataset = [];
			
			var createChart = function(title, stitle)
			{
				var monthlyconsumptionschart = new Highcharts.Chart({
				chart: {
						renderTo: 'coalChart',
						type: 'column'
					},
					credits: {
						enabled: false
					},
				
					title: {
						text: title
					},
					
					subtitle: {
						text: stitle
					},
					
					xAxis: {
						categories: [
							'Gen',
							'Feb',
							'Mar',
							'Apr',
							'Mag',
							'Giu',
							'Lug',
							'Ago',
							'Set',
							'Ott',
							'Nov',
							'Dic'
						],
						crosshair: true
					},
					yAxis: {
						title: {
							text: 'Emissioni CO2 (t)'
						}
					},
					tooltip: {
						headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
						pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
							'<td style="padding:0"><b>{point.y:.1f} t</b></td></tr>',
						footerFormat: '</table>',
						shared: true,
						useHTML: true
					},
					plotOptions: {
						column: {
							pointPadding: 0.2,
							borderWidth: 0
						}
					},
					series: $scope.dataset
					
					
				});
			}
							

			var getdata = function (par, type) 
			{
					var sTitle = "Emissioni risparmiate CO2 nell' anno " + CreemSettings.selectedYear;
					var data = [];
					
					if(type == "POD")
						data = MonthlyConsumptionsFactory.getBuildingConsumptionsYear(par,CreemSettings.selectedYear).query( function(){ prepareChart(data, par,sTitle); });
					else if(type == "CLUSTER")
						data = MonthlyConsumptionsFactory.getClusterConsumptionsYear(par,CreemSettings.selectedYear).query( function(){ prepareChart(data, par, sTitle); });
					else if(type == "PROV") 
						data = MonthlyConsumptionsFactory.getCityConsumptionsYear(par,CreemSettings.selectedYear).query( function(){ prepareChart(data, par, sTitle); });							
				
			}
			
			var prepareChart = function(data, title, stitle)
			{					
						var emissioni_risparmiate = [];			
						var consumi_anno = [];
						for(var i=0; i<data.length; i++)
						{
								
								var conversion_factor = 0.41/1000;
								var currentTime = new Date();
								if( data[i].anno == CreemSettings.selectedYear || data[i].anno == CreemSettings.selectedYear - 1)
								{
									consumi_anno.push( conversion_factor*data[i].gen );
									consumi_anno.push( conversion_factor*data[i].feb );
									consumi_anno.push( conversion_factor*data[i].mar );
									consumi_anno.push( conversion_factor*data[i].apr );
									consumi_anno.push( conversion_factor*data[i].mag );
									consumi_anno.push( conversion_factor*data[i].giu );
									consumi_anno.push( conversion_factor*data[i].lug );
									consumi_anno.push( conversion_factor*data[i].ago );
									consumi_anno.push( conversion_factor*data[i].sett );
									consumi_anno.push( conversion_factor*data[i].ott );
									consumi_anno.push( conversion_factor*data[i].nov );
									consumi_anno.push( conversion_factor*data[i].dic );
												
									
								}
								
								
								
											
						}	
						for(var i=0; i<12; i++)						
									emissioni_risparmiate.push(consumi_anno[i] - consumi_anno[i + 12]);
						
						
						
						$scope.dataset[0] = {
										name: "Emissioni risparmiate",
										data: emissioni_risparmiate
							};

												
						
						createChart(title, stitle);											
			}

			var refreshData = function ()
			{										
					$scope.dataset.splice(0, $scope.dataset.length);		
					
					if(CreemSettings.selectedbuildings.length > 0)
						getdata(CreemSettings.selectedbuildings[0].pod, "POD");
					else if(CreemSettings.selectedCities.length > 0)
						getdata(CreemSettings.selectedCities[0], "PROV");
					else if(CreemSettings.selectedClusters.length > 0)
						getdata(CreemSettings.selectedClusters[0], "CLUSTER");
					
					
			}
			
			$scope.$on('settingsUpdated', function (event, data) { refreshData(); } );
			
			if(CreemSettings.selectedbuildings.length > 0 || CreemSettings.selectedCities.length > 0 || CreemSettings.selectedClusters.length > 0)
				refreshData();
   

		}],
		
		templateUrl: 'directives/coal-chart.html',
		scope: true
  };
  
});
	
	

