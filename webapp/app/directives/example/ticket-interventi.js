CREEMapp.directive('ticketInterventi', function() {
	
	  return {
		scope: {},
	  
		templateUrl: 'directives/ticket-interventi.html',
				
		controller: ['$scope', 'ManutenzioneFactory', 'CreemSettings','$compile', 'uiCalendarConfig', 'TooltipTicketService' ,
		function($scope, ManutenzioneFactory, CreemSettings, $compile, uiCalendarConfig,TooltipTicketService) {	
			
			var getdata = function (par, type) 
			{
				$scope.showModal = false;
				var tickets =[];
				var building = '';
				if (CreemSettings.selectedbuildings.length > 0)					
					building = CreemSettings.selectedbuildings[0].codice;
				
				$('#calendar').fullCalendar('removeEvents');
					
				var data =  ManutenzioneFactory.getTicket().query(
					function()
					{
						tickets = [];
						$scope.events=[];
						var max_ticket =100;		
						
						for(var i=0; i<data.length; i++) {	
							if (building==data[i].codiceImmobile) {
								var date_init = (data[i].dataInizioIntervento).split('-');
								var Y_init = date_init[0];
								var M_init = date_init[1]-1;
								var d_init = date_init[2].split(' ');
								var inizio = new Date(Y_init,M_init,d_init[0]) ;
								var d_fine = parseInt(inizio.getDate()) + parseInt(data[i].durata) ;
								var m_fine = inizio.getMonth();
								var y_fine = inizio.getFullYear() ;
								tickets.push({id:i, title:data[i].descrizione, start: inizio , end: new Date(y_fine, m_fine, d_fine) });
								var mycolor = '#00B0C0';
								if (data[i].priorita=='1') 
									mycolor = '#FF2030';
									
								$('#calendar').fullCalendar('renderEvent', {color: mycolor, id:i, title:data[i].cdIntervento + " - " + data[i].codiceImmobile, start: inizio },true);
					
							}
						}
					}
						
				);

				/* config object */
				$scope.uiConfig = {
				  calendar:{
					height: 650,
					editable: true,
					defaultDate: new Date(moment().subtract(1,'years').format('YYYY'), moment().format('MM') - 1 , moment().format('DD')),
					defaultView: 'month',
					eventClick: function( date, allDay, jsEvent, view ) { 
						var tooltip = data[date.id].descrizione;
						var modalOptions = {
							actionButtonText: 'OK',
							headerText: 'Ticket N° '+ data[date.id].cdIntervento,
							responsabile: data[date.id].buildingManager,
							utenteInserimento: data[date.id].utenteInserimento,
							descrizione: data[date.id].descrizione,
							dataInizioIntervento: data[date.id].dataInizioIntervento,
							dataLimite: data[date.id].dataLimite,
							durata: data[date.id].durata,
							//immobile: data[date.id].denominazione,
							immobile: CreemSettings.selectedbuildings[0].denominazione,
							co2: data[date.id].kgCO2Risparmiati
						};

						TooltipTicketService.showModal({}, modalOptions);
					},
					
					header:{
						right: 'prev, next'
					},
					eventDrop: $scope.alertOnDrop,
					eventResize: $scope.alertOnResize,
					eventRender: $scope.eventRender
				  }
				};
			 
				/* event sources array*/
				$scope.eventSources = tickets;
			}
			
			var refreshData = function ()
			{										
					$scope.eventSources = [];
				
					if(CreemSettings.selectedbuildings.length > 0)
						getdata(CreemSettings.selectedbuildings[0].codice);			
					
			}
			
			$scope.$on('settingsUpdated', function (event, data) { refreshData(); } );
			
			if(CreemSettings.selectedbuildings.length > 0)
				refreshData();

		}		

	],
		  }
	});