CREEMapp.directive('manutenzioneOrdinariaTable', 
function() 
{
	return {    
			templateUrl: 'directives/manutenzione-ordinaria-table.html',
			
			controller: ['$scope', 'ManutenzioneFactory', 'ngTableParams', 'LoadingFactory', 'CreemSettings', 
			
			function($scope, ManutenzioneFactory, ngTableParams, LoadingFactory, CreemSettings) 
			{		
				var filter_immobile = "";
				var filter_cluster = "";
				var filter_prov = "";
				$scope.allItemsSelected = false;
				
				if(CreemSettings.selectedbuildings.length > 0)				
						filter_immobile = CreemSettings.selectedbuildings[0].codice;

				
				var interventi = [{"id":"Sostituzione", "title": "Sostituzione" }];
				var filter_codice = "";
				var filter_cluster = "";
				var filter_prov = "";
				var filter_tipologia = "";
				
				if(CreemSettings.selectedbuildings.length > 0)					
						filter_codice = CreemSettings.selectedbuildings[0].codice;
						
				
				// var m_data =  ManutenzioneFactory.getTipoIntervento().query(
						// function()
						// {
							// if (m_data.length > 0) {
								// interventi.push(  {"id": "" , "title": "Tutti" } );
								// for(var i=0; i<m_data.length; i++) {
									// interventi.push({"id":m_data[i].descrizione, "title": m_data[i].descrizione });
								// }
								
							// }
						// }
				// );
				
				
				var createTable = function(cod, cluster, prov, intervento)
				{
					var my_data = [];
					var j=0;
					for(var i=0; i<data.length; i++) {	
						if (cod!="" && data[i].codice==cod) {
							
							var object = data[i];
							//object.checkBox = '<input type="checkbox" ng-model="checkboxes.items[user.id]" />';
							my_data[j]= object ;
							j++;	
						}
						else if (cluster!="" && data[i].clusterEnergy==cluster) {
							my_data[j]=data[i];
							j++;	
						}
						else if (prov!="" && data[i].siglaprov==prov) {
							my_data[j]=data[i];
							j++;	
						}				
					}
					$scope.tableManutenzione = new ngTableParams({
							page: 1, // show first page
							count: 10, // count per page,							
							}, {
								data: my_data
					});							
				}
	
				var data =  ManutenzioneFactory.getALLBuildingsInterventi().query(
					function() 
					{	console.log(data);
						for(var i=0; i<data.length; i++) {						
							data[i]["idData"] = i;
							if (data[i]["tipologiaIntervento"]==10)
								data[i]["tipologiaIntervento"] = "Sostituzione";
								
								data[i]["statoAttuale"] = "Modello: " + data[i].modVecchioComp +
														  "\rPrestazione: " + data[i].prestazioniVecchioComp +
														  "\rNum. Cambio: " + data[i].numCompDaCambiare +
											              "\rConsumo Totale(W): " + data[i].consumoTotOld + 
											              "\rCosto (€): " + data[i].costoTotOld ;
											
								data[i]["statoProposto"] = "Modello: " + data[i].modNuovoComp + 
														   "\rPrestazione: " + data[i].prestazioniNuovoComp + 
														   "\rNum. Cambio: " + data[i].numComponentiDaComprare + 
														   "\rPotenza (W): " + data[i].potenza + 
														   "\rValore Economico (€): " + data[i].valSingoloComp +  
														   "\rConsumo Tot. (W): " + data[i].consumoTotComponenti +  
														   "\rCosto Tot. (€): " + data[i].costoComponenti ;
							
						}	
						createTable(filter_codice, filter_cluster, filter_prov, "");

					});

				$scope.cols = [
					{ field: "checkBox", title: "", filter: { checkBox: "directives/manutenzione-ordinaria-filter-checkbox.html" },show: true},
					{ field: "clusterEnergy", title: "Cluster"},
					{ field: "tipologiaIntervento", title: "Tipo Intervento", filter: { tipologiaIntervento: "select" }, filterData: interventi, show: true},				
					{ field: "ubicazione", title: "Ubicazione" },
					{ field: "modVecchioComp", title: "Modello In Uso", tooltip: "statoAttuale"},
					{ field: "modNuovoComp", title: "Modello Proposto", tooltip: "statoProposto"},
					{ field: "costoComponenti", title: "Costo Tot (€)"},
					{ field: "consumoTotComponenti", title: "Consumo Tot.(W)"},
					{ field: "numComponentiDaComprare", title: "N°Comp."},
					{ field: "idImmobile", title: "idImmobile",show:false},
					{ field: "denominazione", title: "denominazione",show:false}
				];

				
				$scope.$on('settingsUpdated', function (event, data) { 
					if(CreemSettings.selectedbuildings.length > 0)	{					
						createTable(CreemSettings.selectedbuildings[0].codice, "", "");
						$scope.cols = [
							{ field: "checkBox", title: "", filter: { checkBox: "directives/manutenzione-ordinaria-filter-checkbox.html" },show: true},
							{ field: "clusterEnergy", title: "Cluster"},
							{ field: "tipologiaIntervento", title: "Tipo Intervento", filter: { tipologiaIntervento: "select" }, filterData: interventi, show: true},				
							{ field: "ubicazione", title: "Ubicazione" },
							{ field: "modVecchioComp", title: "Modello In Uso", tooltip: "statoAttuale"},
							{ field: "modNuovoComp", title: "Modello Proposto", tooltip: "statoProposto"},
							{ field: "costoComponenti", title: "Costo Tot (€)"},
							{ field: "consumoTotComponenti", title: "Consumo Tot.(W)"},
							{ field: "numComponentiDaComprare", title: "N°Comp."},
							{ field: "idImmobile", title: "idImmobile",show:false},
							{ field: "denominazione", title: "denominazione",show:false}
						];	
					}
					if(CreemSettings.selectedClusters.length > 0) {
						createTable("", CreemSettings.selectedClusters[0], "");
						$scope.cols = [
							{ field: "checkBox", title: "", filter: { checkBox: "directives/manutenzione-ordinaria-filter-checkbox.html" },show: true},
							{ field: "codice", title: "Codice", filter: { codice: "text" }, show: true , sortable:"'codice'"},
							{ field: "tipologiaIntervento", title: "Tipo Intervento", filter: { tipologiaIntervento: "select" }, filterData: interventi, show: true},				
							{ field: "ubicazione", title: "Ubicazione" },
							{ field: "modVecchioComp", title: "Modello In Uso", tooltip: "statoAttuale"},
							{ field: "modNuovoComp", title: "Modello Proposto", tooltip: "statoProposto"},
							{ field: "costoComponenti", title: "Costo Tot (€)"},
							{ field: "consumoTotComponenti", title: "Consumo Tot.(W)"},
							{ field: "numComponentiDaComprare", title: "N°Comp."},
							{ field: "idImmobile", title: "idImmobile",show:false},
							{ field: "denominazione", title: "denominazione",show:false}
						];	
					}
					if(CreemSettings.selectedCities.length > 0)  {
						createTable("", "", CreemSettings.selectedCities[0]);
						$scope.cols = [
							{ field: "checkBox", title: "", filter: { checkBox: "directives/manutenzione-ordinaria-filter-checkbox.html" },show: true},
							{ field: "codice", title: "Codice", filter: { codice: "text" }, show: true , sortable:"'codice'"},
							{ field: "tipologiaIntervento", title: "Tipo Intervento", filter: { tipologiaIntervento: "select" }, filterData: interventi, show: true},				
							{ field: "ubicazione", title: "Ubicazione" },
							{ field: "modVecchioComp", title: "Modello In Uso", tooltip: "statoAttuale"},
							{ field: "modNuovoComp", title: "Modello Proposto", tooltip: "statoProposto"},
							{ field: "costoComponenti", title: "Costo (€)"},
							{ field: "consumoTotComponenti", title: "Consumo Tot.(W)"},
							{ field: "numComponentiDaComprare", title: "N°Comp."},
							{ field: "idImmobile", title: "idImmobile",show:false},
							{ field: "denominazione", title: "denominazione",show:false}
						];	
					}
				} );
       
			$scope.selectAll = function()
			{
	            
				if($scope.allItemsSelected)
					$scope.allItemsSelected=false;
				else
					$scope.allItemsSelected = true;
					
	            for (var i = 0; i < $scope.tableManutenzione.data.length; i++) {
	                $scope.tableManutenzione.data[i].isChecked = $scope.allItemsSelected;
	            }
	        };
			
			$scope.generaTicket = function()
			{
				var tickets = [];
				
				for (var i = 0; i < $scope.tableManutenzione.data.length; i++) {
	                if($scope.tableManutenzione.data[i].isChecked)
					{
						var ticket = {
						"idImmobile" : $scope.tableManutenzione.data[i].idImmobile,
						"codiceImmobile" : $scope.tableManutenzione.data[i].codice,
						"denominazioneUp" : $scope.tableManutenzione.data[i].denominazione,
						"descrizione" : "Ubicazione: " + $scope.tableManutenzione.data[i].ubicazione +
										"Sostituzione " + $scope.tableManutenzione.data[i].numCompDaCambiare + " " +
										$scope.tableManutenzione.data[i].modVecchioComp + " con " +
										$scope.tableManutenzione.data[i].numComponentiDaComprare + " " +
										$scope.tableManutenzione.data[i].modNuovoComp,
						"dataRichiesta" : setDate(),
						"categoria" : 5,
						"tipoTicket" : "MO",
						"idImpComp" : $scope.tableManutenzione.data[i].id,
						"priorita" : 0,
						"provincia" : setProvincia($scope.tableManutenzione.data[i].codice),
						"prioritaStr" : "ORDINARIA"
						};
						
						tickets.push(ticket);
					}
	            }
				
				ManutenzioneFactory.insertTicket(tickets);
				
			};
			
			var setProvincia = function(codiceImmobile)
			{
				var provincia = codiceImmobile.substr(0,2);
				var nPro;
				switch(provincia) {
					case 'AG' : nPro=  1000; break;
					case 'CL' : nPro=  1001; break;
					case 'CT' : nPro=  1002; break;
					case 'EN' : nPro=  1003; break;
					case 'ME' : nPro=  1004; break;
					case 'PA' : nPro=  1005; break;
					case 'RG' : nPro=  1006; break;
					case 'SR' : nPro=  1007; break;
					case 'TP' : nPro=  1008; break;
					default : nPro= 1000
				}
				
				return nPro;
			}
			
			var setDate = function()
			{
				var today = new Date();
				var dd = today.getDate();
				var mm = today.getMonth()+1; //January is 0!

				var yyyy = today.getFullYear();
				if(dd<10){
					dd='0'+dd
				} 
				if(mm<10){
					mm='0'+mm
				} 
				var today = yyyy+'-'+mm+'-'+dd;
				return today;
			}

		}],

  };
 
});
