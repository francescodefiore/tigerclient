CREEMapp.directive('kpiPerformance', 

function() 
{
	
	return {    
	scope: {},
    controller: ['$scope', 'CreemSettings', 'KPIFactory',
	
		function($scope, CreemSettings, KPIFactory) 
		{			
							
			
			var createGauge = function(title, unit, divchart, kpivalue, bench, tollerance)
			{
				
				var budgetchart = new Highcharts.Chart({
				
				chart: {
					polar: true,
					type: 'line',
					renderTo: divchart
				},
				credits: {
					enabled: false
				},

				title: {
							text: title,
							style:{								
								fontSize: '15px'
							}
				},
				
				subtitle: {
							text: unit
				},


				xAxis: {
					categories: ['Gen', 'Feb', 'Mar', 'Apr', 'Mag',	'Giu', 'Lug', 'Ago', 'Set', 'Ott', 'Nov', 'Dic'],
					tickmarkPlacement: 'on',
					lineWidth: 0
				},

				yAxis: {
					gridLineInterpolation: 'polygon',
					lineWidth: 0,
					min: 0
				},

				tooltip: {
					shared: true,
					//pointFormat: '<span style="color:{series.color}">{series.name}: <b>{point.y:,.00f}</b><br/>'
					pointFormatter: function(){
					
						var value = Number(this.y);
						if(value > 1)
							value = Math.round(value);
						
						var text = '<span style="color:' + this.series.color + '">' + this.series.name + ': <b>'+ value + '</b><br/>';
						
						return text;
					}
					
				},

				series: [{
					name: 'KPI',
					data: kpivalue,
					color: 'rgba(0,0,0,1)',
					pointPlacement: 'on'
				}, {
					name: 'Benchmark',
					type: 'area',
					color: 'rgba(51, 153, 102,1)',
					marker: { enabled: false },
					data: bench,
					pointPlacement: 'on'
				}]
				
    
				
				
						});
			}					


			var refreshData = function ()
			{		
					$scope.building = CreemSettings.selectedbuildings[0];
					var currentTime = new Date();
					var current_year = currentTime.getFullYear();
					//KPI anno precedente
					var data = KPIFactory.getYearlyKPI($scope.building.idImmobile, current_year - 1).query(
						function()
						{
							var kpi = data;
							var potenza_picco = [];
							var consumo_medio = [];
							
							var bench_potenza_picco = [];
							var bench_consumo_medio = [];
							
							var tollerance_potenza_picco = [];
							var tollerance_consumo_medio = [];
							
							if(kpi.length > 0)
							{
								for(var i=0; i<kpi.length; i++)
								{
									if( data[i].Kpi == "KpiMensileReqF001" )
									{
										potenza_picco.push( data[i].valoreKpi );
										bench_potenza_picco.push(data[i].valoreBench);
										tollerance_potenza_picco.push(data[i].tolleranza);
									
									}
									if(data[i].Kpi == "KpiMensileReqF002")
									{
										consumo_medio.push( data[i].valoreKpi );
										bench_consumo_medio.push( data[i].valoreBench );
										tollerance_consumo_medio.push( data[i].tolleranza );										
									}
								}
								
								
								
							}
								
							console.log("refresh kpi");
							console.log(kpi);
							
							createGauge("Potenza di Picco", "kW", "polar_potenzapicco", potenza_picco, bench_potenza_picco, tollerance_potenza_picco);
							createGauge("Consumo Medio al mq", "kWh", "polar_consumomedio", consumo_medio, bench_consumo_medio, tollerance_consumo_medio);
							
							
						}
					
					);

			}
			
			$scope.$on('settingsUpdated', function (event, data) { refreshData(); } );
			
			if(CreemSettings.selectedbuildings.length > 0 || CreemSettings.selectedCities.length > 0 || CreemSettings.selectedClusters.length > 0)
				refreshData();
	
			
			
		}],
		
		templateUrl: 'directives/kpi-performance.html'
  };
  
});
	
	