'use strict';

CREEMapp.service('LoginService', ['$rootScope','$location','Base64','RESTservAddr','$http','$cookies',
					function ($rootScope, $location,Base64,RESTservAddr,$http,$cookies) {
    var state = {
        isLogged: false,
        username: undefined,
		itemMenu: []
    };
	
	var urlBase = '/anagrafica';
	
    this.signIn = function (credentials) {
        if ( !credentials.username || !credentials.password ) {
            throw 'Nessuna password o username specificati.';
        }

		var ut = {username:credentials.username};
		var parameter = JSON.stringify(ut);
		
		var config = {headers: {
			  'Access-Control-Allow-Origin': '*'
			  }
			};
		
		
		$http.post(RESTservAddr + urlBase + "/login", parameter,config).
				success(function(data, status, headers, config) {
					console.log(data);
					
					if(data.length == 0)
						$rootScope.$broadcast('loginErrorEvent');
						
					else if(Base64.decode(data[0].password) == credentials.password)
					{
						state.isLogged = true;
						state.username = credentials.username;
						/*var now = new Date();
						now.setMinutes(now.getMinutes() + 1);
						console.log(now);
						$cookies.putObject(credentials.username,data, {expires: now});*/
						state.itemMenu = data[0].itemMenu;
						$rootScope.$broadcast('loginSuccessfulEvent');
						$location.path('/dashboard');
					}
					
				  }).
				  error(function(data, status, headers, config) {
					//LoadingFactory.stopLoading();
				  });
		
        /*if ( (credentials.username === 'creem') && credentials.password === 'softeco') {
            state.isLogged = true;
            state.username = credentials.username;
            $rootScope.$broadcast('loginSuccessfulEvent');
            $location.path('/dashboard');
        }
        else {
            throw 'Password or username errati';
        }*/
    };

    this.signOut = function () {
        state.isLogged = false;
		state.username = undefined;
        $rootScope.$broadcast('logoutSuccessfulEvent');
		$location.path('/login');		
    };

    this.isLoggedIn = function () {
        return state.isLogged;
    };

    this.getUsername = function() {
        return state.username;
    }
	
	this.getItemMenu = function(){
	
		return state.itemMenu;
	}

}]);