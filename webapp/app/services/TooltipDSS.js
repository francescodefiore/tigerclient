CREEMapp.service('TooltipDSS', ['$uibModal','$cookies','CreemSettings',
    function ($uibModal,$cookies,CreemSettings) {

        var modalDefaults = {
            backdrop: true,
            keyboard: true,
            modalFade: true,
            templateUrl: 'directives/tooltipdss.html'
        };

        this.showModal = function (customModalDefaults, customModalOptions) {
           
			var modalInstance = $uibModal.open({
			animation: true,
			templateUrl: 'directives/tooltipdss.html',
			controller: function ($scope, $uibModalInstance) {
			
							var tempModalOptions = {};
							$scope.ok = function () {
								$uibModalInstance.close();
							};

							$scope.cancel = function () {
								$uibModalInstance.dismiss('cancel');
							};
							  
							$scope.modalOptions = {
								actionButtonText: '',
								closeButtonText: 'Chiudi'            
							};
							
							angular.extend(tempModalOptions, $scope.modalOptions, customModalOptions);
							$scope.modalOptions = tempModalOptions;
						},
			//size: size,
			/*resolve: {
			items: function () {
				  return $scope.items;
				}
			  }*/
			});

			modalInstance.result.then(function () {
			  $cookies.putObject('building',CreemSettings.selectedbuildings[0]);
			});
        };
    }]);