CREEMapp.factory('BusDataFactory', ['RESTservAddr','$resource','LoadingFactory','$http', 

	function(RESTservAddr, $resource, LoadingFactory, $http) 
	{

			var urlBase = '/daticampo';
			var BusDataFactory = {};
			var pesb = [];
			var energyMeter = [];
			var thermalMeter = [];
			
			BusDataFactory.getSensorData = function(idImmobile) 
			{

				LoadingFactory.startLoading();
								
				return $resource(RESTservAddr + urlBase + "/sensordata?idImmobile=" + idImmobile, {}, 
				{
					query: {
						method: 'GET',
						isArray : true,
						cache: true,										
						interceptor: {
							response: function (data) {										
								LoadingFactory.stopLoading();												
							},
							responseError: function (data) {
								console.log("error");
								LoadingFactory.stopLoading();
							}
						}					
					}							
				});
								
			}
			
			BusDataFactory.getEnergyData = function(idImmobile) 
			{

				LoadingFactory.startLoading();
								
				return $resource(RESTservAddr + urlBase + "/energydata?idImmobile=" + idImmobile, {}, 
				{
					query: {
						method: 'GET',
						isArray : true,
						cache: true,										
						interceptor: {
							response: function (data) {										
								LoadingFactory.stopLoading();												
							},
							responseError: function (data) {
								console.log("error");
								LoadingFactory.stopLoading();
							}
						}					
					}							
				});
								
			}
			
			BusDataFactory.getBuildingBoards = function(idImmobile,tipoScheda) 
			{

				LoadingFactory.startLoading();
								
				return $resource(RESTservAddr + urlBase + "/schede?idImmobile=" + idImmobile + "&tipoScheda=" + tipoScheda, {}, 
				{
					query: {
						method: 'GET',
						isArray : true,
						cache: true,										
						interceptor: {
							response: function (data) {										
								LoadingFactory.stopLoading();												
							},
							responseError: function (data) {
								console.log("error" + data);
								LoadingFactory.stopLoading();
							}
						}					
					}							
				});
								
			}
			
			BusDataFactory.getBoardsData = function(parpesb,parenergy,parthermal,q) 
			{

				LoadingFactory.startLoading();
								
				return $resource(RESTservAddr + urlBase + "/datipesb", {}, 
				{
					query: {
						method: 'GET',
						isArray : true,
						//cache: true,
						params:{
							ips:parpesb.ipLocazione,
							ipf:parpesb.schedaFenomeno,
							da:parpesb.da,
							a:parpesb.a,
							ipl:parenergy.ipLocazione,
							ipe:parenergy.ipSchedaEnergy,
							cole:parenergy.colonnaDb,
							iptl:parthermal.ipLocazione,
							ipt:parthermal.ipSchedaEnergy,
							colt:parthermal.colonnaDb,
							q:q
						},
						interceptor: {
							response: function (data) {										
								LoadingFactory.stopLoading();												
							},
							responseError: function (data) {
								console.log("error" + data);
								LoadingFactory.stopLoading();
							}
						}					
					}							
				});
								
			}
			
			/*BusDataFactory.getPesbData = function(parpesb) 
			{

				LoadingFactory.startLoading();
								
				//return $resource(RESTservAddr + urlBase + "/datipesb?ips=" + ips + "&ipf=" + ipf + "&da=" + da + "&a=" + a, {}, 
				return $http( 
				{
						url: RESTservAddr + urlBase + "/datipesb",
						method: 'POST',
						dataType: 'json',
						headers: {
									'Content-type': 'application/json'
								},
						data:parpesb,			
				}).then(function successCallback(response){
					return response;
				});
								
			}*/
			
			
			
			BusDataFactory.addPesb = function(newObj) {
					  pesb = [];
					  pesb.push(newObj);
				  };

			  BusDataFactory.getPesb = function(){
				  return pesb;
			  };
			  
			  BusDataFactory.addEnergyMeter = function(newObj) {
					  energyMeter = [];
					  energyMeter.push(newObj);
				  };

			  BusDataFactory.getEnergyMeter = function(){
				  return energyMeter;
			  };
			  
			  BusDataFactory.addThermalMeter = function(newObj) {
					  thermalMeter = [];
					  thermalMeter.push(newObj);
				  };

			  BusDataFactory.getThermalMeter = function(){
				  return thermalMeter;
			  };
			
			
			
			return BusDataFactory;
	}
]);




