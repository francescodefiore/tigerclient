CREEMapp.factory("AppSettings", ['$rootScope', function ($rootScope) {
    
	var settingsObject;

	
    settingsObject = {
	
        selectdShops: [],

        selectedClusters: [],
		
		selectedCities: [],
		
		numInstances: [],

		selectedWeather: '',
		
		selectedMeasures: '',
		
		selectedSuperficieDss: '',
		
        selectedDates: { from: moment().subtract(1,'year').subtract(7, 'days').format("YYYY-MM-DD"), to: moment().subtract(1,'year').format("YYYY-MM-DD") },
		
		selectedDatesCampo: { from: moment().subtract(7, 'days').format("YYYY-MM-DD"), to: moment().format("YYYY-MM-DD") },
		
		selectedYear: '',
		
		reset: function()
		{
			this.selectdShops = [];
            this.selectedClusters = [];         
			this.selectedCities = [];
			this.selectedMeasures= '';
			this.selectedSuperficieDss = '';
			this.selectedDates.from = moment().subtract(1,'year').subtract(7, 'days').format("YYYY-MM-DD");
			this.selectedDates.to = moment().subtract(1,'year').format("YYYY-MM-DD");
			//this.selectedDatesCampo.from = moment().subtract(7, 'days').format("YYYY-MM-DD");
			//this.selectedDatesCampo.to = moment().format("YYYY-MM-DD");
		},
		
        setSettings: function (conf) 
		{
			this.reset();
            this.selectdShops = conf.buildings;
            this.selectedClusters = conf.clusters;         
            this.selectedDates = conf.dates;
			this.selectedDatesCampo = conf.datescampo;
			this.selectedWeather = conf.weather;
			this.selectedMeasures= conf.measures;
			this.selectedSuperficieDss = conf.selectedSuperficieDss;
			this.notifyObservers();
			
			
        },
		
		setShops: function(shop)
		{
			this.reset();
			this.selectdShops.push(shop);			
			this.notifyObservers();
		},
		
		setCity: function(city)
		{
			this.reset();
			this.selectedCities[0] = city;			
			this.notifyObservers();
		},
		
		setCluster: function(cluster)
		{
			this.reset();		
			this.selectedClusters[0] = cluster;			
			this.notifyObservers();
		},
		
		setMeasure: function(measure)
		{
			this.reset();		
			this.selectedMeasures = measure;			
			this.notifyObservers();
		},
		
		setSuperficieDss: function(supdss)
		{
			this.reset();		
			this.selectedSuperficieDss = supdss;			
			this.notifyObservers();
		},
		
		setBoards: function(numInstances)
		{
			if(this.numInstances.length == (numInstances == 0 ? 0 : numInstances-1))
			{
				this.numInstances = [];
				this.notifyObserverBoards();
			}
			else
				this.numInstances.push(1);
		},
		
		setYear: function(year)
		{
			this.selectedYear = year;
		},
		
		observerCallbacks: [],
			
		notifyObservers: function(){
			
			$rootScope.$broadcast('settingsUpdated', {});
				
		},
		
		notifyObserverBoards: function(){
			
			$rootScope.$broadcast('setBoards', {});
				
		}

    };
	
	//settingsObject.reset();
	
    return settingsObject;
}]);