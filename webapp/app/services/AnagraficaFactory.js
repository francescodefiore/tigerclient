
CREEMapp.factory('AnagraficaFactory', ['RESTservAddr','$resource','LoadingFactory', 


	function(RESTservAddr, $resource, LoadingFactory) 
	{

		var urlBase = '/anagrafica';
		var AnagraficaFactory = {};

		
		AnagraficaFactory.getAllShops = function() {

			LoadingFactory.startLoading();
			

			return $resource(RESTservAddr + urlBase + "/getAllNegozi", {}, {
								query: {
									method: 'GET',
									isArray : true,
									cache: true,
									interceptor: {
										response: function (data) {										
											LoadingFactory.stopLoading();
										},
										responseError: function (data) {
											LoadingFactory.stopLoading();
										}
									}								
								}							
							});
		}
		
		return AnagraficaFactory;
	}
]);



CREEMapp.factory("BuildingForm", function(RESTservAddr, $resource)
		{			
			var urlBase = RESTservAddr + '/immobile';
			return $resource(urlBase, {}, {
				update: {
					method: 'POST'
				}
			});
		}
);


