
CREEMapp.factory('MonthlyConsumptionsFactory', ['RESTservAddr','$resource','LoadingFactory','$http', 

	function(RESTservAddr, $resource, LoadingFactory, $http) 
	{

			var urlBase = '/consuntivi';
			var MonthlyConsumptionsFactory = {};

			MonthlyConsumptionsFactory.getMonthlyConsumptions = function() 
			{
				
				LoadingFactory.startLoading();
				
				return $resource(RESTservAddr + urlBase, {}, 
								{
									query: {
										method: 'GET',
										isArray : true,
										cache: true,										
										interceptor: {
											response: function (data) {										
												LoadingFactory.stopLoading();
											},
											responseError: function (data) {
												LoadingFactory.stopLoading();
											}
										}							
									}							
								});
								
			}
			
			MonthlyConsumptionsFactory.getBuildingConsumptionsYear = function(pod,year) 
			{
				
				LoadingFactory.startLoading();
				
				return $resource(RESTservAddr + urlBase + "/anno?filterBy=POD&filterParams=" + pod + "&year=" + year, {}, 
								{
									query: {
										method: 'GET',
										isArray : true,
										cache: true,										
										interceptor: {
											response: function (data) {										
												LoadingFactory.stopLoading();
											},
											responseError: function (data) {
												LoadingFactory.stopLoading();
											}
										}							
									}							
								});
								
			}
			
			
			MonthlyConsumptionsFactory.getBuildingConsumptions = function(pod) 
			{
				
				LoadingFactory.startLoading();
				
				return $resource(RESTservAddr + urlBase + "?filterBy=POD&filterParams=" + pod, {}, 
								{
									query: {
										method: 'GET',
										isArray : true,
										cache: true,										
										interceptor: {
											response: function (data) {										
												LoadingFactory.stopLoading();
											},
											responseError: function (data) {
												LoadingFactory.stopLoading();
											}
										}							
									}							
								});
								
			}
			
			MonthlyConsumptionsFactory.getCityConsumptionsYear = function(city,year) 
			{
				
				LoadingFactory.startLoading();
				
				return $resource(RESTservAddr + urlBase + "/anno?groupBy=PROV&groupByParams=" + city + "&year=" + year, {}, 
								{
									query: {
										method: 'GET',
										isArray : true,
										cache: true,										
										interceptor: {
											response: function (data) {										
												LoadingFactory.stopLoading();
											},
											responseError: function (data) {
												LoadingFactory.stopLoading();
											}
										}							
									}							
								});
								
			}
			
			MonthlyConsumptionsFactory.getCityConsumptions = function(city) 
			{
				
				LoadingFactory.startLoading();
				
				return $resource(RESTservAddr + urlBase + "?groupBy=PROV&groupByParams=" + city, {}, 
								{
									query: {
										method: 'GET',
										isArray : true,
										cache: true,										
										interceptor: {
											response: function (data) {										
												LoadingFactory.stopLoading();
											},
											responseError: function (data) {
												LoadingFactory.stopLoading();
											}
										}							
									}							
								});
								
			}
			
			MonthlyConsumptionsFactory.getClusterConsumptionsYear = function(cluster,year) 
			{
				
				LoadingFactory.startLoading();
				
				return $resource(RESTservAddr + urlBase + "/anno?groupBy=CLUSTERENERGY&groupByParams="  + cluster + "&year=" + year, {}, 
								{
									query: {
										method: 'GET',
										isArray : true,
										cache: true,										
										interceptor: {
											response: function (data) {										
												LoadingFactory.stopLoading();
											},
											responseError: function (data) {
												LoadingFactory.stopLoading();
											}
										}							
									}							
								});
								
			}
			
			MonthlyConsumptionsFactory.getClusterConsumptions = function(cluster) 
			{
				
				LoadingFactory.startLoading();
				
				return $resource(RESTservAddr + urlBase + "?groupBy=CLUSTERENERGY&groupByParams=" + cluster, {}, 
								{
									query: {
										method: 'GET',
										isArray : true,
										cache: true,										
										interceptor: {
											response: function (data) {										
												LoadingFactory.stopLoading();
											},
											responseError: function (data) {
												LoadingFactory.stopLoading();
											}
										}							
									}							
								});
								
			}
			
			
			
			return MonthlyConsumptionsFactory;
	}
]);


