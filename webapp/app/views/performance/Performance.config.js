'use strict';

CREEMapp.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/performance', {
    templateUrl: 'views/performance/PerformanceView.html'
  });
}]);