'use strict';

CREEMapp.config(['$routeProvider', function($routeProvider) {
  
  $routeProvider
  
	.when('/input/utentigruppi', {	
		templateUrl: 'views/input/UtentiGruppiView.html'	
	})
	
	.when('/input/import', {	
		templateUrl: 'views/input/ImportView.html',
        controller: 'ImportController'
	})
	
	.when('/input/building', {	
		templateUrl: 'views/input/BuildingView.html'        
	})
	
	.when('/input/generic', {	
		templateUrl: 'views/input/GenericView.html'        
	})
	
	.when('/input/impianti', {
		templateUrl: 'views/input/PlantsView.html',
	});
  
  
}]);